package app_contactList.appClasses;

import java.util.Arrays;

public class Contact implements Comparable {
	
	private String[] phoneNr;
	private String[] eMail;
	
	public Contact(String[] phoneNr, String[] eMail) {
		
		this.phoneNr = phoneNr;
		this.eMail = eMail;
	}

	public String[] getPhoneNr() {
		return phoneNr;
	}

	public void setPhoneNr(String[] phoneNr) {
		this.phoneNr = phoneNr;
	}

	public String[] geteMail() {
		return eMail;
	}

	public void seteMail(String[] eMail) {
		this.eMail = eMail;
	}

	@Override
	public String toString() {
		return "Contact [phoneNr=" + Arrays.toString(phoneNr) + ", eMail=" + Arrays.toString(eMail) + "]";
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}
